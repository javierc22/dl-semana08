# ------------ Ejercicio de integración -------------- #
# El usuario ingresa un producto y un precio, se debe cambiar
# el precio del producto ingresado en el archivo

product_name = 'Product1'
price = 800

file = File.open('Ejemplo-15.txt', 'r')
products = file.readlines.map(&:chomp)
file.close

products = products.map { |e| e.split(' ') }.to_h
products[product_name] = price
print products # => {"Product1"=>"800", "Product2"=>"210", "Product3"=>"400"}

file = File.open('Ejemplo-15.txt', 'w')
products.each do |key, value|
  file.puts "#{key} => #{value}"
end
file.close