# -------------- ARCHIVOS -------------- #

# El método .read devuelve un string con todo el contenido dentro del archivo.
file = File.open('sample.txt', 'r')
contents = file.read
file.close

puts contents

# Forma de bloque:
# En el bloque el archivo se cierra de forma automática.
File.open( 'sample.txt', 'r') { |file| puts file.read }
