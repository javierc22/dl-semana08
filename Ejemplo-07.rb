# ------------- DESAFIO SUMA -------------- #
# Con el siguiente archivo, abrirlo y sumar todos los valores de la 3era columna
file = File.open('Ejemplo-07.txt', 'r')
table = file.readlines
file.close

suma = table.inject(0) { |sum, line| sum + line.split(" ")[2].to_i }
average = suma / table.length.to_f
puts suma
