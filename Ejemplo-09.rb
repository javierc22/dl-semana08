# ------------- MULTIPLES LINEAS -------------- #
file = File.open('Ejemplo-09.txt', 'r')
movies = file.readlines
file.close

movies.each_slice(5) do |slice|
 print slice[0]
 print slice[1]
 print slice[2]
 print slice[3]
 print slice[4]
end

# each_slice(n) => Itera el bloque dado para cada segmento de <n> elementos.
# Si no se proporciona ningún bloque, devuelve un enumerador.