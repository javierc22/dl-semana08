# -------------- PROMEDIO CON INJECT -------------- #

file = File.open('sample.txt', 'r')
data = file.readlines.map(&:chomp) # => ["Producto1 100", "Producto2 250", "Producto3 720"]
file.close

suma = data.inject(0) { |sum, product| sum + product.split(' ')[1].to_i }

average = suma / data.length
puts average # => 356