# ------------- PELICULA MAS VOTADA -------------- #

file = File.open('Ejemplo-09.txt', 'r')
movies = file.readlines
file.close

votes = []
movies.each_slice(5) do |movie|
  votes << movie[4].split(', ').map(&:chomp) # << = .push()
end

print votes.map { |movie| movie.count('good') } # => [4, 6, 8, 6, 6, 9]