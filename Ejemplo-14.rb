# ------------- ESCRIBIENDO UN ARCHIVO -------------- #
file = File.open('hola.txt', 'w') # w = write
file.puts 'Holaaaaaaa!'
file.close

# Forma de bloque:
# File.open("sample.txt", "w"){ |file| file.puts "Hello file!"}