# -------------- READLINES -------------- #
# La clase File tiene un método llamado '.readlines' que devuelve de forma automática
# el contenido separado por línea.

file = File.open('sample.txt', 'r')
data = file.readlines.map(&:chomp) # => ["Producto1 100", "Producto2 250", "Producto3 720"]
file.close

print data