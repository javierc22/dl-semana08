## Desafío Latam - Semana 08
#### Manejo de archivos

1. Introducción
2. Archivos
3. Contando líneas
4. Readlines
5. Promedio
6. Promedio con inject
7. Scope y bloques en archivos
8. Desafío suma
9. Desafío suma resuelto
10. Listado de palabras
11. Múltiples líneas
12. Splat
13. Contando votos
14. Película más votada
15. Desafío pokémon
16. Solución desafío pokémon
17. Escribiendo en un archivo
18. Ejercicio de integración
19. Append
20. File exist
21. Excepciones
22. Antipatrones
23. Quiz