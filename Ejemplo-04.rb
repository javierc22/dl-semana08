# -------------- PROMEDIO -------------- #

file = File.open('sample.txt', 'r')
data = file.readlines.map(&:chomp) # => ["Producto1 100", "Producto2 250", "Producto3 720"]
file.close

suma = 0
data.each do |product|
  suma += product.split(' ')[1].to_i
end

average = suma / data.length
puts average # => 356
