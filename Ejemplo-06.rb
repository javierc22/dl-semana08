# ------------- SCOPE Y BLOQUES EN ARCHIVOS -------------- #
# En este caso para poder imprimer 'a', primero debe declararse antes del bloque
# ya que si sólo se declara en el bloque, al querer imprimir, saldrá error.
a = nil
File.open('sample.txt', 'r') { |file| a = file.readlines }
puts a
