# ----------- APPEND ------------ #
# Si queremos escribir en un archivo sin borrar lo anterior,
# utilizaremos 'append', de esta forma podemos abrir un archivo y escribir al final
File.open('myfile.txt', 'a') { |f| f.puts 'Hello world' } # a = append