# ----------- FILE EXIST ------------ #
# Para verificar si un archivo o directorio existe, podemos ocupar el método '.exists?'

if file.exists?(filename)
  puts "#{filename} existe!"
end