# ------------- LISTADO DE PALABRAS -------------- #
# Se tiene un archivo con un listado de palabras
# Crear un método para determinar si una palabra se encuentra dentro de
# un archivo con un listado de palabras.
def scanner(filename, word)
  file = File.open( filename, 'r')
  data = file.readlines.map(&:chomp)
  file.close
  data.include?(word)
end

puts scanner('Ejemplo-08.txt', 'abomasum')