# ------------- SPLAT -------------- #
def show(name, date, studio, category, votes)
  puts name
  puts date
end

file = File.open('Ejemplo-09.txt', 'r')
movies = file.readlines
file.close

movies.each_slice(5) do |movie|
 show(*movie) # operador splat
end

# El operador splat nos permite pasar un arreglo como si
# fueran parámetros separados, de esta forma obtenemos
# un código mucho más ordenado
